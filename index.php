<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="public/styles/bootstrap.min.css">
    <link href="public/styles/normalize.css" rel="stylesheet">
    <link href="public/styles/styles.css" rel="stylesheet">
    <title>MRW Client</title>
</head>
<body class="bg-dark">
<div class="container mt-5">
    <h1 class="text-info text-center mb-5"><span class="tile-remark"></span>MySQL <span class="tile-remark">TRACKING APP</span></h1>
    <div class="mb-3">
        <label class="form-label text-white">Número pedido: </label>
    </div>
    <div class="mb-3">
        <input id="input_pedido" class="form-control" type="text" placeholder="Número pedido"/>
    </div>
    <div class="mb-3">
        <button id="btn_consultar" type="button" class="btn btn-success mt-3">Consultar</button>
    </div>
    
    <!-- LOADER -->
    <div id="loading"></div>

    <div id="contenedor_estado" class="mt-5 hide">
        <div class="mb-3">
            <label class="form-label text-white">Estado pedido: </label>
        </div>
        <div class="mb-3">
            <input id="estado_pedido" class="form-control text-light estado" type="text"/>
        </div>
    </div>

    <div class="mt-5 bg-secondary text-white">
        <pre id="input_pre" class="p-3 text-info">
        </pre>
    </div>
    <div id="contenedor_reset" class="mb-3 hide">
        <button id="btn_reset" type="button" class="btn btn-warning mt-3">Borrar</button>
    </div>
</div>
</div>
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"></script> -->
<script type="text/javascript" src="public/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="public/js/codeJS.js"></script>
</body>
</html>
