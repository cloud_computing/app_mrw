<?php 

$config = require_once("config.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['pedido'])) {
        $pedido = $_POST['pedido'];
        $connection = conectaDB($config);
        $query = "SELECT * FROM Pedidos WHERE pedidoID = ?";
        $result = $connection->prepare($query);
        $result->execute(array($pedido));
        $data = $result->fetchall(PDO::FETCH_ASSOC);
        if($data === false){
            die("Error en la Consulta a la BBDD.");
        }else if($data === 0){
            $data['EstadoDescripcion'] = 'El pedido no existe en la Base de Datos';
            //$arrResponse = array('status' => false, 'msg' => 'Sin resultados', 'data' => $data);
            $arrResponse = array('status' => false, 'msg' => 'Sin resultados', 'data' => $data);
        }else{
            /* foreach ($data as $key => $value) {
                var_dump($value);
            } */
            $arrResponse = array('status' => true, 'msg' => 'Datos del pedido obtenidos con éxito.', 'data' => $data);
        }
    }else {
        $data['EstadoDescripcion'] = 'El pedido no existe en la Base de Datos';
        $arrResponse = array('status' => false, 'msg' => 'Sin resultados', 'data' => $data);
        // print_r("No existen suficientes datos para realizar la consulta solicitada!");
    }
} else {
    $data['EstadoDescripcion'] = 'El pedido no existe en la Base de Datos';
    $arrResponse = array('status' => false, 'msg' => 'Sin resultados', 'data' => $data);
    // print_r("No existen suficientes datos para realizar la consulta solicitada!");
}
echo json_encode($arrResponse, JSON_UNESCAPED_UNICODE);


/**
 * Abre una conexión con la BBDD MySQL
 * @param array array con los datos de acceso a la BBDD
 * @return object objeto de conexión a la BBDD
 */
function conectaDB(array $config){
    $host = $config["mysql_host"];
    $dbname = $config["mysql_dbname"];
    $user = $config["mysql_user"];
    $password = $config["mysql_password"];
    try {
        $conn = new PDO("mysql:host=$host;dbname=$dbname", $user, $password);
        // echo "Connected to $dbname at $host successfully.";
        return $conn;
    } catch (PDOException $pe) {
        die("Could not connect to the database $dbname :" . $pe->getMessage());
    }
}


?>