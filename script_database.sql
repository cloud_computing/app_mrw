CREATE TABLE Pedidos (
    PedidoID int,
    Aplicacion varchar(255),
    NombreAbonado varchar(255),
    Estado int,
    EstadoDescripcion varchar(255)
);

INSERT INTO Pedidos (PedidoID, Aplicacion, NombreAbonado, Estado, EstadoDescripcion)
VALUES ('0', 'SAGEC', 'ELIX PHARMA', '17', 'Recogido');
