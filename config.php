<?php 
$config = [
    "mysql_host" => "YOUR HOST",
    "mysql_dbname" => "YOUR DATABASE",
    "mysql_user" => "YOUR USER",
    "mysql_password" => "YOUR PASSWORD",
];
return $config;
?>