// referencias Elementos DOM
const btn_consultar = document.getElementById('btn_consultar');
const btn_borrar = document.getElementById('btn_reset');
const input_pedido = document.getElementById('input_pedido');
const estado_pedido = document.getElementById('estado_pedido');
const input_pre = document.getElementById('input_pre');
const contenedor_reset = document.getElementById('contenedor_reset');
const contenedor_estado = document.getElementById('contenedor_estado');
const loader = document.querySelector("#loading");

/**
 * Consulta al Servicio Web MRW si existe un pedido con la finalidad de obtener sus datos de tracking
 */
btn_consultar.addEventListener('click', () => {

    displayLoading();

    //console.clear();

    // Nos aseguramos de ocultar el botón reset antes de que se devuelva un resultado de consulta
    if(!contenedor_reset.classList.contains('hide')){
        contenedor_reset.classList.add('hide');
    }
    // Vaciamos el contenido del input estado y, después, lo ocultamos
    estado_pedido.value = '';
    if(!contenedor_estado.classList.contains('hide')){
        contenedor_estado.classList.add('hide');
    }
    // Vaciamos el contenido del input de datos del pedido
    input_pre.innerHTML = '';

    let n_pedido = input_pedido.value;

    // Definimos la URL para la consulta al Servicio Web
    let url = "http://{YOUR WEB SERVER: YOUR PORT}/MySQL_Client/sql.php";
    let formData = new FormData();
    formData.append('pedido', n_pedido);
    fetch(url, {
        method: 'POST',
        body: formData
    })
    .then(res=>res.json())
    .then(data=>{
        console.table('data.data[0]', data.data[0].EstadoDescripcion);
        console.log('-----------------------')
        if (data.status === true) {
            if (input_pre.classList.contains('text-warning')) {
                input_pre.classList.remove('text-warning');
            }
            if(data.data[0]['EstadoDescripcion']){
                if(contenedor_estado.classList.contains('hide')){
                    contenedor_estado.classList.remove('hide');
                }
                estado_pedido.value = data.data[0].EstadoDescripcion.toUpperCase();
            }
            hideLoading();
            for (property in data.data[0]) {
                console.log(property + ': ' + data.data[0][property]);
                input_pre.innerHTML += property + ': ' + data.data[0][property] + '<br>';
            }
        } else if (data.status === false) {
            if (!input_pre.classList.contains('text-warning')) {
                input_pre.classList.add('text-warning');
            }
            hideLoading();
            input_pre.innerHTML = data['msg'];
        }
        if (contenedor_reset.classList.contains('hide')) {
            contenedor_reset.classList.remove('hide');
        }
    }).catch(error=>{
        console.log(error)
        return false;
    });
});

/**
 * Resetea los valores de la interfaz para preparar una nueva consulta
 */
btn_reset.addEventListener('click', () => {
    input_pedido.value = '';
    input_pre.innerHTML = '';
    if(!contenedor_reset.classList.contains('hide')){
        contenedor_reset.classList.add('hide')
    }
    estado_pedido.value = '';
    if(!contenedor_estado.classList.contains('hide')){
        contenedor_estado.classList.add('hide');
    }
});

// showing loading
function displayLoading() {
    loader.classList.add("display");
    // to stop loading after some time
    setTimeout(() => {
        loader.classList.remove("display");
    }, 5000);
}

// hiding loading 
function hideLoading() {
    loader.classList.remove("display");
}
